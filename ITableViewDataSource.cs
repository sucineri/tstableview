﻿namespace Tacticsoft
{
    /// <summary>
    /// An interface for a data source to a TableView
    /// </summary>
    public interface ITableViewDataSource
    {
        /// <summary>
        /// Get the number of rows that a certain table should display
        /// </summary>
        int GetNumberOfRowsForTableView();
        
        /// <summary>
        /// Get the height of a row of a certain cell in the table view
        /// </summary>
        float GetHeightForRowInTableView(int row);

        string GetReuseIdentifierForRow(int row);

        void PopulateCellForRow(TableViewCell cell, int row);

        TableViewCell InstantiateCellForRow(int row);
    }
}

