﻿using UnityEngine;

namespace Tacticsoft
{
    /// <summary>
    /// The base class for cells in a TableView. ITableViewDataSource returns pointers
    /// to these objects
    /// </summary>
    public class TableViewCell : MonoBehaviour
    {
        private string _reuseIdentifier;
        private float? _height;

        /// <summary>
        /// TableView will cache unused cells and reuse them according to their
        /// reuse identifier. Override this to add custom cache grouping logic.
        /// </summary>
        public virtual string ReuseIdentifier
        { 
            get
            { 
                if (string.IsNullOrEmpty(_reuseIdentifier))
                {
                    _reuseIdentifier = GetType().Name;
                }
                return _reuseIdentifier;
            } 
        }

        public virtual float Height
        {
            get
            {
                if (_height == null)
                {
                    _height = GetComponent<RectTransform>().rect.height;
                }
                return _height.Value;
            }
        }
    }
}
